---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '11.01.2024'
pubDate: 2024-01-17
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
Info streigi kohta: veebileht CTF TECH

Vahur selgitas kuidas töötab DDoS rünnak. Vahur näitab eelmise päeva küberuudiseid (andmelekked, pahavara, uuendused, tarkvara nõrkused, hackerite vahistamine jne).


Filmi vaatamine

Teemaks kuidas inimesi luuratakse. Näiteks vanasti olid hotellides mikrofonid. Räägitakse kuidas tänapäeval luurab valitsus sinu nutiseadmetega, sinu tegevusi. Valitsuse organisatsioonidel on võimalik ligi pääseda igale poole kuhu nad tahavad ja arvatakse, te valitsus on teadnud enamustest terrorirünnakutest, aga pole midagi teinud nende ärahoidmisest.


Rasmus Reimo konverentsi vaatamine

Seletatakse termineid näiteks (Dos, DDoS, swatting). Räägitakse kuidas üks isik lõi saidi, mis rääkis häkkimisest ja häkkeritest. Ühele häkkerigruppile see ei meeldinud ja nad üritasid seda maha võtta saates koju SWAT tiimi ja sooritades DDoS attacki.   
