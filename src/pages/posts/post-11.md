---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '8.02.24'
pubDate: 2024-02-14
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
Õppisime teksti krüpteerimist, näiteks ACSII süsteem. Tegime hands on teksti erinevates krüpteeringutes tõlkimist. 
