---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '9.01.2024'
pubDate: 2024-01-13
description: 'See on küberkaitse blogi'
author: 'Siim'
tags: ["blogging"]
---
Küberkaitse teemalise filmi vaatamine (kübersõja oht). Esimene küberrünnak stuxnet, millega üritati maha võtta Irani uraani tootmise tehast ja tuumajaami.
