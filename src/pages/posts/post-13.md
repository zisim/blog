---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '14.02.24'
pubDate: 2024-02-15
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
Lahendasime tiimiga arvestuse ülesandeid. Otsime lehe koodi seest võtmeid. Uurime vanasid lehtesid, unzipime faile kus sees on võti.
