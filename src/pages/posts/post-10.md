---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '7.02.24'
pubDate: 2024-02-14
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
Üritasime teha ülesandeid eelmises tunnis õpitu kohta, aga lehel toimus võrgukatkestus.
Parandasime oma tetrise mängu. Kuulasime infot Jaapani õpilaste tehtud töö kohta.
