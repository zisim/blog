---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '13.02.24'
pubDate: 2024-02-14
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
Õppisime kuidas teksti krüpeerida kasutades hashi. Õppisime milliseid sisselogimise viise peaksime kasutama. Uurisime infot Mari Roostiku kohta.
