---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '17.01.24'
pubDate: 2024-01-17
description: 'Küberkaitse blogi'
author: 'Siim'
image:
    url: 'https://gitlab.com/zisim/blog/-/raw/main/public/image.png'
    alt: 'Pilt'

tags: ["küberkaitse", "blogging"]
---
Valge maja tüngakõne

X-tee eesti ühiskond (riigi teenused erinevates serverites)

Tornide “lagunemine”

Haigekassa uus nimi - “tervisekassa”

Transpordiamet haldab umbes 1000 serverit.

Video “X-tee tutvustus(pikem versioon)”

X-tee loodi 2001

Eesti.ee lehe teenused (minu sõidukid, ravikindlustus, põhikooli hinded)

Moodle testi tegemine

E kirjad ja nendega pettused

Gaijin.at/en/tools/ - lehel tööriistade kasutamine

Kõrgtehnoloogilised lahendused väravaga 
