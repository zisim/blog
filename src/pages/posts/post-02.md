---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '10.01.2024'
pubDate: 2024-01-14
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
#Kursuse ülesehitus

Õppimismeetodid:
    Loengud

Lehtedele ligipääs (Moodle) - kõik ei saa ligi, eriti kasutades telefoni, sest 

teenusepakkuja kasutamine muudab su teise riiki, kust pole sisenemine lubatud. Lahendus sellele VPN 

#Film (kübersõda)

Filmis oli juttu sellest kuidas Venemaa häkkerid ründavad oma kodust ohutult Ukraina hädavajalikku taristut. Ukraina küberkaitse spetsialistid seletavad kuidas neid rünnatakse ja mida nad teevad, et ennast kaitsta.  
