---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '16.01.24'
pubDate: 2024-01-17
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
Vahuri mesilase jutt- kes on sees see on sees ja kes ei ole on väljas

Vahuri kirjastiil ja araabia keele seos

Küberkaitse vajalikkus:

Riistvara - tuumajaam

Andmed - valimine(Kristiina Ojuland), isiku teesklemine

Õpetaja valimisreklaami mitte tegemine

Darkweb ja silk road(paha asi)

Mängukontode müük

Küberuudised(gitlabi turvanõrkus, microsofti uuendused, Mississipi lunavara jne)

Kiri ja kirjaoskus läbi aja

Infoallikad läbi aja: köster- ema- õpetaja-sõber- sotsiaalmeedia

Info juurdetulek ja piiratud ligipääs nt hinded

Küberturvalisus tähendab informatsiooni ja seda sisaldavate seadmete kaitset erinevate ohtude ja rünnakute eest.

Kuberturvalisuse raames on olulisel kohal kolm infoturbe eesmarki: käideldavus, terviklikkus, konfidentsiaalsus

Video “Mis on küberturvalisus” Raul rikk

Kaardimäng

Vahuri lasteaialood

Runescape pulmad ja pettused

Ründed- andmekadu, pahavara, veebi ründed, identiteedivargus ja sotsiaalrunded

Top 10 web application security risks

Vahuri isikukood- 37104132714

Turvaklassid K0(koige vahem turvalik) - K3

Veebi jagunemine surfav, sügav, dark

Testi tegemine
