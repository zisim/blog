---
layout: '../../layouts/MarkdownPostLayout.astro'
title: '30.01.24'
pubDate: 2024-02-14
description: 'Küberkaitse blogi'
author: 'Siim'
tags: ["küberkaitse", "blogging"]
---
CTF ülesannete tegemine. Diretory’itest flagide leidmine, pakitud failide avamine ja nendest flag’ide leidmine.
